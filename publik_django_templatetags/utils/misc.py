# publik-django-templatetags
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.parse

from django.conf import settings


def get_known_service_for_url(url):
    netloc = urllib.parse.urlparse(url).netloc
    for services in settings.KNOWN_SERVICES.values():
        for service in services.values():
            remote_url = service.get('url')
            if urllib.parse.urlparse(remote_url).netloc == netloc:
                return service
    return None
