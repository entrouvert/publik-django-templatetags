# publik-django-templatetags
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import datetime
import math
import re
import urllib.parse
from decimal import Decimal
from decimal import DivisionByZero as DecimalDivisionByZero
from decimal import InvalidOperation as DecimalInvalidOperation

from django import template
from django.template import defaultfilters
from django.utils.encoding import force_str

from publik_django_templatetags.publik import utils

register = template.Library()


@register.filter(name='get')
def get(obj, key):
    try:
        return obj.get(key)
    except AttributeError:
        try:
            return obj[key]
        except (IndexError, KeyError, TypeError):
            return None
    except TypeError:
        return None


@register.filter
def getlist(mapping, key):
    if mapping is None:
        return []
    mapping = list(mapping)
    for value in mapping:
        try:
            yield value.get(key)
        except AttributeError:
            yield None


@register.filter(name='list')
def as_list(obj):
    try:
        return list(obj)
    except TypeError:
        return []


@register.filter
def split(string, separator=' '):
    return (force_str(string) or '').split(separator)


@register.filter
def removeprefix(string, prefix=None):
    if not string:
        return ''
    value = force_str(string)
    prefix = force_str(prefix)
    return value.removeprefix(prefix)


@register.filter
def removesuffix(string, suffix=None):
    if not string:
        return ''
    value = force_str(string)
    suffix = force_str(suffix)
    return value.removesuffix(suffix)


@register.filter
def first(value):
    try:
        return defaultfilters.first(value)
    except (TypeError, KeyError):
        return ''


@register.filter
def last(value):
    try:
        return defaultfilters.last(value)
    except (TypeError, KeyError):
        return ''


@register.filter
def startswith(string, substring):
    return string and force_str(string).startswith(force_str(substring))


@register.filter
def endswith(string, substring):
    return string and force_str(string).endswith(force_str(substring))


def parse_decimal(value, default=Decimal(0), do_raise=False):
    if isinstance(value, str):
        # replace , by . for French users comfort
        value = value.replace(',', '.')
    try:
        return Decimal(value).quantize(Decimal('1.0000')).normalize()
    except (ArithmeticError, TypeError):
        if do_raise:
            raise
        return default


@register.filter(is_safe=False)
def decimal(value, arg=None):
    if not isinstance(value, Decimal):
        value = parse_decimal(value)
    if arg is None:
        return value
    return defaultfilters.floatformat(value, arg=arg)


@register.filter
def add(term1, term2):
    '''replace the "add" native django filter'''

    # consider None content as the empty string
    if term1 is None:
        term1 = ''
    if term2 is None:
        term2 = ''

    # return available number if the other term is the empty string
    if term1 == '':
        try:
            return parse_decimal(term2, do_raise=True)
        except (ArithmeticError, TypeError):
            pass
    if term2 == '':
        try:
            return parse_decimal(term1, do_raise=True)
        except (ArithmeticError, TypeError):
            pass

    # compute addition if both terms are numbers
    try:
        return parse_decimal(term1, do_raise=True) + parse_decimal(term2, do_raise=True)
    except (ArithmeticError, TypeError, ValueError):
        pass

    # append to term1 if term1 is a list and not term2
    if isinstance(term1, list) and not isinstance(term2, list):
        return list(term1) + [term2]

    # fallback to django add filter
    return defaultfilters.add(term1, term2)


@register.filter
def subtract(term1, term2):
    return parse_decimal(term1) - parse_decimal(term2)


@register.filter
def multiply(term1, term2):
    return parse_decimal(term1) * parse_decimal(term2)


@register.filter
def divide(term1, term2):
    try:
        return parse_decimal(term1) / parse_decimal(term2)
    except DecimalInvalidOperation:
        return ''
    except DecimalDivisionByZero:
        return ''


@register.filter
def ceil(value):
    '''the smallest integer value greater than or equal to value'''
    return decimal(math.ceil(parse_decimal(value)))


@register.filter
def floor(value):
    return decimal(math.floor(parse_decimal(value)))


@register.filter(name='abs')
def abs_(value):
    return decimal(abs(parse_decimal(value)))


@register.filter(name='sum')
def sum_(list_):
    if isinstance(list_, str):
        # do not consider string as iterable, to avoid misusage
        return ''
    try:
        return sum(parse_decimal(term) for term in list_)
    except TypeError:  # list_ is not iterable
        return ''


@register.filter
def clamp(value, minmax):
    try:
        value = parse_decimal(value, do_raise=True)
        min_value, max_value = (parse_decimal(x, do_raise=True) for x in minmax.split())
    except (ArithmeticError, TypeError, ValueError):
        return ''
    return max(min_value, min(value, max_value))


@register.filter
def limit_low(value, min_value):
    try:
        return max(parse_decimal(value, do_raise=True), parse_decimal(min_value, do_raise=True))
    except (ArithmeticError, TypeError):
        return ''


@register.filter
def limit_high(value, max_value):
    try:
        return min(parse_decimal(value, do_raise=True), parse_decimal(max_value, do_raise=True))
    except (ArithmeticError, TypeError):
        return ''


@register.filter(is_safe=False)
def duration(value, arg='short'):
    if arg not in ('short', 'long'):
        return ''
    # value is expected to be a timedelta or a number of minutes
    if not isinstance(value, datetime.timedelta):
        try:
            value = datetime.timedelta(seconds=int(value) * 60)
        except (TypeError, ValueError):
            return ''
    return utils.seconds2humanduration(int(value.total_seconds()), short=bool(arg != 'long'))


@register.filter(name='list')
def list_(value):
    # turn a generator into a list
    if isinstance(value, collections.abc.Iterable) and not isinstance(value, (collections.abc.Mapping, str)):
        return list(value)
    else:
        return [value]


@register.filter
def with_auth(value, arg):
    parsed_url = urllib.parse.urlparse(value)
    new_netloc = '%s@%s' % (arg, parsed_url.netloc.rsplit('@', 1)[-1])
    return urllib.parse.urlunparse(parsed_url._replace(netloc=new_netloc))


@register.filter
def housenumber_number(housenumber):
    match = re.match(r'^\s*([0-9]+)(.*)$', force_str(housenumber))
    if not match:
        return ''
    number, btq = match.groups()
    return number


@register.filter
def housenumber_btq(housenumber):
    match = re.match(r'^\s*([0-9]+)(.*)$', force_str(housenumber))
    if not match:
        return ''
    number, btq = match.groups()
    return btq.strip()
