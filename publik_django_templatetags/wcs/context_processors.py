# publik-django-templatetags
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.utils.dateparse import parse_date, parse_datetime
from django.utils.http import urlencode
from django.utils.timezone import is_naive, make_aware
from requests.exceptions import RequestException

from publik_django_templatetags.utils.requests_wrapper import requests
from publik_django_templatetags.wcs.utils import get_wcs_services


def get_default_wcs_service_key():
    services = get_wcs_services()

    for key, service in services.items():
        if not service.get('secondary', False):
            # if secondary is not set or not set to True, return this one
            return key

    return None


class LazyFormDefObjectsManager:
    api_base_url = 'api/forms'

    def __init__(
        self,
        service_key,
        form_id,
        custom_view_id=None,
        filters=None,
        geo_center=None,
        user=Ellipsis,
        django_request=None,
    ):
        self._service_key = service_key
        self._form_id = form_id
        self._custom_view_id = custom_view_id

        self._filters = filters or {}
        self._user = user
        self._geo_center = geo_center or {}

        self._cached_resultset = None
        self._django_request = django_request
        self._cache_duration = 5

    def _clone(self):
        return self.__class__(
            service_key=self._service_key,
            form_id=self._form_id,
            custom_view_id=self._custom_view_id,
            filters=self._filters,
            geo_center=self._geo_center,
            user=self._user,
            django_request=self._django_request,
        )

    def none(self):
        qs = self._clone()
        qs._cached_resultset = []
        return qs

    def order_by(self, attribute):
        qs = self._clone()
        qs._filters['order_by'] = attribute
        if not attribute:
            del qs._filters['order_by']
        return qs

    def with_custom_view(self, custom_view_id):
        qs = self._clone()
        qs._custom_view_id = custom_view_id
        return qs

    def get_full(self):
        qs = self._clone()
        qs._filters['full'] = 'on'
        return qs

    def include_fields(self):
        qs = self._clone()
        qs._filters['include-fields'] = 'on'
        return qs

    def include_evolution(self):
        qs = self._clone()
        qs._filters['include-evolution'] = 'on'
        return qs

    def include_roles(self):
        qs = self._clone()
        qs._filters['include-roles'] = 'on'
        return qs

    def include_submission(self):
        qs = self._clone()
        qs._filters['include-submission'] = 'on'
        return qs

    def include_workflow(self):
        qs = self._clone()
        qs._filters['include-workflow'] = 'on'
        return qs

    def include_workflow_data(self):
        qs = self._clone()
        qs._filters['include-workflow-data'] = 'on'
        return qs

    def get_at(self, value):
        qs = self._clone()
        if isinstance(value, str):
            parsed = parse_datetime(value)
            if not parsed:
                parsed = parse_date(value)
            if parsed:
                value = parsed
        if isinstance(value, datetime.datetime):
            if is_naive(value):
                value = make_aware(value)
            value = value.isoformat()
        elif isinstance(value, datetime.date):
            value = make_aware(datetime.datetime.combine(value, datetime.datetime.min.time())).isoformat()
        if value:
            qs._filters['at'] = value
        return qs

    def access_control(self, user):
        qs = self._clone()
        qs._user = user
        return qs

    @property
    def count(self):
        return len(self)

    def first(self):
        qs = self._clone()
        qs._filters['limit'] = 1
        try:
            return list(qs)[0]
        except IndexError:
            return ''

    def filter_by(self, attribute):
        qs = self._clone()
        qs.pending_attr = attribute
        return qs

    def apply_filter_value(self, value):
        assert self.pending_attr
        qs = self._clone()
        if value is None:
            value = ''
        if isinstance(value, bool):
            value = str(value).lower()
        op = getattr(self, 'pending_op', 'eq')
        if self.pending_attr in ['internal_id', 'number', 'identifier', 'user', 'status', 'distance']:
            return getattr(self, 'filter_by_%s' % self.pending_attr)(value, op)
        qs._filters['filter-%s' % self.pending_attr] = value
        qs._filters['filter-%s-operator' % self.pending_attr] = op
        return qs

    def apply_op(self, op):
        self.pending_op = op
        return self

    def apply_eq(self):
        return self.apply_op('eq')

    def apply_ne(self):
        return self.apply_op('ne')

    def apply_ieq(self):
        return self.apply_op('ieq')

    def apply_lt(self):
        return self.apply_op('lt')

    def apply_lte(self):
        return self.apply_op('lte')

    def apply_gt(self):
        return self.apply_op('gt')

    def apply_gte(self):
        return self.apply_op('gte')

    def apply_in(self):
        return self.apply_op('in')

    def apply_not_in(self):
        return self.apply_op('not_in')

    def apply_between(self):
        return self.apply_op('between')

    def apply_absent(self):
        self.apply_op('absent')
        return self.apply_filter_value('on')

    def apply_existing(self):
        self.apply_op('existing')
        return self.apply_filter_value('on')

    def apply_is_today(self):
        self.apply_op('is_today')
        return self.apply_filter_value('on')

    def apply_is_tomorrow(self):
        self.apply_op('is_tomorrow')
        return self.apply_filter_value('on')

    def apply_is_yesterday(self):
        self.apply_op('is_yesterday')
        return self.apply_filter_value('on')

    def apply_is_this_week(self):
        self.apply_op('is_this_week')
        return self.apply_filter_value('on')

    def filter_by_internal_id(self, internal_id, op='eq'):
        qs = self._clone()
        if internal_id:
            qs._filters['filter-internal-id'] = internal_id
            qs._filters['filter-internal-id-operator'] = op
        return qs

    def filter_by_number(self, number, op='eq'):
        qs = self._clone()
        if number:
            qs._filters['filter-number'] = number
        return qs

    def filter_by_identifier(self, identifier, op='eq'):
        qs = self._clone()
        if identifier:
            qs._filters['filter-identifier'] = identifier
        return qs

    def filter_by_user(self, user, op='eq'):
        qs = self._clone()
        if user:
            if hasattr(user, 'is_authenticated') and hasattr(user, 'get_name_id'):
                if user.is_authenticated and user.get_name_id():
                    qs._filters['filter-user-uuid'] = user.get_name_id()
            elif isinstance(user, str):
                qs._filters['filter-user-uuid'] = user
        if 'filter-user-uuid' not in qs._filters:
            return qs.none()
        return qs

    def filter_by_status(self, status, op='eq'):
        qs = self._clone()
        if status:
            qs._filters['filter'] = status
            if op in ['eq', 'ne']:
                qs._filters['filter-operator'] = op
        return qs

    def filter_by_distance(self, distance, op=None):
        # distance do not currently support an operator
        qs = self._clone()
        if distance:
            qs._filters['filter-distance'] = distance
        return qs

    def set_geo_center_lat(self, lat):
        qs = self._clone()
        qs._geo_center['center_lat'] = lat
        return qs

    def set_geo_center_lon(self, lon):
        qs = self._clone()
        qs._geo_center['center_lon'] = lon
        return qs

    def set_cache_duration(self, cache_duration):
        qs = self._clone()
        qs._cache_duration = cache_duration
        return qs

    def _get_results_from_wcs(self):
        service = get_wcs_services().get(self._service_key)
        if not service:
            return []

        api_url = '%s/%s/list' % (self.api_base_url, self._form_id)
        if self._custom_view_id:
            api_url += '/%s' % self._custom_view_id
        if self._filters:
            query = urlencode(self._filters)
            api_url += '?%s' % query
            if 'center_lat' in self._geo_center and 'center_lon' in self._geo_center:
                api_url += '&' + urlencode(self._geo_center)
        without_user = self._user is Ellipsis  # not set
        try:
            response = requests.get(
                api_url,
                remote_service=service,
                user=None if without_user else self._user,
                without_user=without_user,
                log_errors=False,
                cache_duration=self._cache_duration,
                django_request=self._django_request,
            )
            response.raise_for_status()
        except RequestException:
            return []

        response_data = response.json()

        if isinstance(response_data, list):
            return response_data

        if response_data.get('err') == 1:
            return []

        return response_data.get('data') or []

    def _populate_cache(self):
        if self._cached_resultset is not None:
            return
        self._cached_resultset = self._get_results_from_wcs()

    def __len__(self):
        self._populate_cache()
        return len(self._cached_resultset)

    def __getitem__(self, key):
        try:
            if not isinstance(key, slice):
                int(key)
        except ValueError:
            raise TypeError
        self._populate_cache()
        return self._cached_resultset[key]

    def __iter__(self):
        self._populate_cache()
        yield from self._cached_resultset

    def __nonzero__(self):
        return any(self)


class LazyCardDefObjectsManager(LazyFormDefObjectsManager):
    api_base_url = 'api/cards'


class LazyFormDef:
    def __init__(self, slug, django_request=None):
        self._django_request = django_request

        if ':' in slug:
            self.service_key, self.form_id = slug.split(':')[:2]
        else:
            self.form_id = slug
            self.service_key = get_default_wcs_service_key()

    @property
    def objects(self):
        return LazyFormDefObjectsManager(self.service_key, self.form_id, django_request=self._django_request)


class LazyCardDef(LazyFormDef):
    @property
    def objects(self):
        return LazyCardDefObjectsManager(self.service_key, self.form_id, django_request=self._django_request)


class Forms:
    def __init__(self, django_request=None):
        self._django_request = django_request

    def __getattr__(self, attr):
        try:
            return LazyFormDef(attr, django_request=self._django_request)
        except KeyError:
            raise AttributeError(attr)


class Cards:
    def __init__(self, django_request=None):
        self._django_request = django_request

    def __getattr__(self, attr):
        try:
            return LazyCardDef(attr, django_request=self._django_request)
        except KeyError:
            raise AttributeError(attr)


def cards(request):
    # legacy
    return {
        'cards': Cards(django_request=request),
    }


def wcs_objects(request):
    return {
        'forms': Forms(django_request=request),
        'cards': Cards(django_request=request),
    }
