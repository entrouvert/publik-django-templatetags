# publik-django-templatetags
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template

from publik_django_templatetags.publik.templatetags import publik

register = template.Library()


@register.filter
def first(queryset):
    try:
        return queryset.first()
    except AttributeError:
        return publik.first(queryset)


@register.filter
def objects(wcs_objects, slug):
    try:
        return getattr(wcs_objects, slug).objects
    except AttributeError:
        return None


@register.filter
def with_custom_view(queryset, custom_view_id):
    try:
        return queryset.with_custom_view(custom_view_id)
    except AttributeError:
        return None


@register.filter
def get_full(queryset):
    try:
        return queryset.get_full()
    except AttributeError:
        return None


@register.filter
def include_fields(queryset):
    try:
        return queryset.include_fields()
    except AttributeError:
        return None


@register.filter
def include_evolution(queryset):
    try:
        return queryset.include_evolution()
    except AttributeError:
        return None


@register.filter
def include_roles(queryset):
    try:
        return queryset.include_roles()
    except AttributeError:
        return None


@register.filter
def include_submission(queryset):
    try:
        return queryset.include_submission()
    except AttributeError:
        return None


@register.filter
def include_workflow(queryset):
    try:
        return queryset.include_workflow()
    except AttributeError:
        return None


@register.filter
def include_workflow_data(queryset):
    try:
        return queryset.include_workflow_data()
    except AttributeError:
        return None


@register.filter
def get_at(queryset, value):
    return queryset.get_at(value)


@register.filter
def access_control(queryset, user):
    try:
        return queryset.access_control(user)
    except AttributeError:
        return None


@register.filter
def count(queryset):
    try:
        return queryset.count
    except AttributeError:
        return 0


@register.filter
def filter_by(queryset, attribute):
    try:
        return queryset.filter_by(attribute)
    except AttributeError:
        return None


@register.filter
def filter_value(queryset, value):
    try:
        return queryset.apply_filter_value(value)
    except AttributeError:
        return None


@register.filter
def filter_by_internal_id(queryset, internal_id):
    try:
        return queryset.filter_by_internal_id(internal_id)
    except AttributeError:
        return None


@register.filter
def filter_by_number(queryset, number):
    try:
        return queryset.filter_by_number(number)
    except AttributeError:
        return None


@register.filter
def filter_by_identifier(queryset, identifier):
    try:
        return queryset.filter_by_identifier(identifier)
    except AttributeError:
        return None


@register.filter
def filter_by_user(queryset, user):
    try:
        return queryset.filter_by_user(user)
    except AttributeError:
        return None


@register.filter
def filter_by_status(queryset, status):
    try:
        return queryset.filter_by_status(status)
    except AttributeError:
        return None


@register.filter
def pending(queryset):
    try:
        return queryset.filter_by_status('pending')
    except AttributeError:
        return None


@register.filter
def done(queryset):
    try:
        return queryset.filter_by_status('done')
    except AttributeError:
        return None


@register.filter(name='equal')
def eq(queryset):
    try:
        return queryset.apply_eq()
    except AttributeError:
        return None


@register.filter(name='not_equal')
def ne(queryset):
    try:
        return queryset.apply_ne()
    except AttributeError:
        return None


@register.filter(name='i_equal')
def ieq(queryset):
    try:
        return queryset.apply_ieq()
    except AttributeError:
        return None


@register.filter(name='less_than')
def lt(queryset):
    try:
        return queryset.apply_lt()
    except AttributeError:
        return None


@register.filter(name='less_than_or_equal')
def lte(queryset):
    try:
        return queryset.apply_lte()
    except AttributeError:
        return None


@register.filter(name='greater_than')
def gt(queryset):
    try:
        return queryset.apply_gt()
    except AttributeError:
        return None


@register.filter(name='greater_than_or_equal')
def gte(queryset):
    try:
        return queryset.apply_gte()
    except AttributeError:
        return None


@register.filter(name='in')
def _in(queryset):
    try:
        return queryset.apply_in()
    except AttributeError:
        return None


@register.filter()
def not_in(queryset):
    try:
        return queryset.apply_not_in()
    except AttributeError:
        return None


@register.filter()
def between(queryset):
    try:
        return queryset.apply_between()
    except AttributeError:
        return None


@register.filter()
def absent(queryset):
    try:
        return queryset.apply_absent()
    except AttributeError:
        return None


@register.filter()
def existing(queryset):
    try:
        return queryset.apply_existing()
    except AttributeError:
        return None


@register.filter()
def is_today(queryset):
    try:
        return queryset.apply_is_today()
    except AttributeError:
        return None


@register.filter()
def is_tomorrow(queryset):
    try:
        return queryset.apply_is_tomorrow()
    except AttributeError:
        return None


@register.filter()
def is_yesterday(queryset):
    try:
        return queryset.apply_is_yesterday()
    except AttributeError:
        return None


@register.filter()
def is_this_week(queryset):
    try:
        return queryset.apply_is_this_week()
    except AttributeError:
        return None


@register.filter
def order_by(queryset, attribute):
    try:
        return queryset.order_by(attribute)
    except AttributeError:
        return None


@register.filter
def filter_by_distance(queryset, distance):
    try:
        return queryset.filter_by_distance(distance)
    except AttributeError:
        return None


@register.filter
def set_geo_center_lat(queryset, lat):
    try:
        return queryset.set_geo_center_lat(lat)
    except AttributeError:
        return None


@register.filter
def set_geo_center_lon(queryset, lon):
    try:
        return queryset.set_geo_center_lon(lon)
    except AttributeError:
        return None


@register.filter
def cache_duration(queryset, duration):
    try:
        duration = float(duration)
    except ValueError:
        return queryset

    try:
        return queryset.set_cache_duration(duration)
    except AttributeError:
        return None
