from pathlib import Path

import nox


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)  # pylint: disable=exec-used
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'psycopg2-binary',
        'WebTest',
        'pytest',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


@nox.session()
@nox.parametrize('django', ['>=4.2,<4.3'])
def tests(session, django):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-django',
        'pytest-freezer',
        django_version=django,
    )

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov',
            'publik_django_templatetags/',
            '--cov-config',
            '.coveragerc',
            '--junitxml',
            f'junit-{django}.xml',
        ]

    if not session.interactive:
        args += ['-v']

    args += session.posargs + ['tests/']

    session.run(*args)


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
