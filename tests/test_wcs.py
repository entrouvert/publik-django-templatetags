import copy
import datetime
import json
from unittest import mock
from urllib.parse import parse_qs, urlparse

import pytest
from django.template import Context, Template
from django.test.client import RequestFactory
from requests.exceptions import ConnectionError, Timeout
from requests.models import Response

from publik_django_templatetags.wcs.context_processors import Cards, Forms


@pytest.fixture
def context():
    request = RequestFactory().get('/')
    ctx = Context(
        {
            'cards': Cards(request),
            'forms': Forms(request),
            'request': request,
        }
    )
    ctx['request'].user = None
    return ctx


class MockAnonymousUser:
    is_authenticated = False
    is_anonymous = True


class MockUser:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False

    def get_name_id(self):
        return None


class MockUserWithNameId:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False

    def get_name_id(self):
        return 'xyz'


class MockedRequestResponse(mock.Mock):
    status_code = 200

    def json(self):
        return json.loads(self.content)


def mocked_requests_send(request, **kwargs):
    data = [{'id': 1, 'fields': {'foo': 'bar'}}, {'id': 2, 'fields': {'foo': 'baz'}}]  # fake result
    if '/api/forms/' in request.url:
        return MockedRequestResponse(content=json.dumps(data))
    return MockedRequestResponse(content=json.dumps({'data': data}))


def test_context(context):
    assert 'cards' in context
    assert 'forms' in context


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_objects(mock_send, wcs_objects, settings, context, nocache):
    # lazy filters
    t = Template('{{ %s|objects:"foo" }}' % wcs_objects)
    assert t.render(context).startswith(
        '&lt;publik_django_templatetags.wcs.context_processors.Lazy%sDefObjectsManager object at'
        % ('Card' if wcs_objects == 'cards' else 'Form')
    )
    assert mock_send.call_args_list == []  # lazy
    t = Template('{{ %s|objects:"default:foo" }}' % wcs_objects)
    assert t.render(context).startswith(
        '&lt;publik_django_templatetags.wcs.context_processors.Lazy%sDefObjectsManager object at'
        % ('Card' if wcs_objects == 'cards' else 'Form')
    )
    assert mock_send.call_args_list == []  # lazy

    # test filters evaluation
    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.1:8999/api/%s/foo/list?' % wcs_objects
    )  # primary service
    t = Template('{{ %s|objects:"default:foo"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.1:8999/api/%s/foo/list?' % wcs_objects
    )
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"other:foo"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.2:8999/api/%s/foo/list?' % wcs_objects
    )
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"unknown:foo"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list == []  # unknown, not evaluated

    # test card_id with variable
    context['foobar'] = 'some-slug'
    mock_send.reset_mock()
    t = Template('{{ %s|objects:foobar|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.1:8999/api/%s/some-slug/list?' % wcs_objects
    )

    # test with no secondary param
    KNOWN_SERVICES = copy.deepcopy(settings.KNOWN_SERVICES)
    KNOWN_SERVICES['wcs'] = {'default': {'url': 'http://127.0.0.3:8999/'}}
    settings.KNOWN_SERVICES = KNOWN_SERVICES
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"bar"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.3:8999/api/%s/bar/list?' % wcs_objects
    )

    mock_send.reset_mock()
    t = Template('{{ %s|get:"bar" }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list == []

    context = Context({})  # nothing in context
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"bar" }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_with_custom_view(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|with_custom_view:"foobar"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.1:8999/api/%s/foo/list/foobar?' % wcs_objects
    )  # primary service
    t = Template('{{ %s|objects:"default:foo"|with_custom_view:"foobar"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.1:8999/api/%s/foo/list/foobar?' % wcs_objects
    )
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"other:foo"|with_custom_view:"foobar"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list[0][0][0].url.startswith(
        'http://127.0.0.2:8999/api/%s/foo/list/foobar?' % wcs_objects
    )
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"unknown:foo"|with_custom_view:"foobar"|list }}' % wcs_objects)
    t.render(context)
    assert mock_send.call_args_list == []  # unknown, not evaluated

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|with_custom_view:"foobar"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_full(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'full=on&' not in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_full|list }}' % wcs_objects)
    t.render(context)
    assert 'full=on&' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|get_full|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_include_filters(mock_send, wcs_objects, context, nocache):
    for part in ['fields', 'evolution', 'roles', 'submission', 'workflow', 'workflow_data']:
        _filter = 'include_%s' % part
        param = _filter.replace('_', '-')
        t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
        t.render(context)
        assert '%s=on&' % param not in mock_send.call_args_list[0][0][0].url
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s|list }}' % (wcs_objects, _filter))
        t.render(context)
        assert '%s=on&' % param in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        context['foobar'] = None
        t = Template('{{ foobar|%s|list }}' % _filter)
        t.render(context)
        assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_at(mock_send, wcs_objects, settings, freezer, context, nocache):
    settings.TIME_ZONE = 'Europe/Paris'
    freezer.move_to('2022-10-17 12:21')

    def get_at_param(url):
        parsed = parse_qs(urlparse(url).query)
        if 'at' not in parsed:
            return
        return parsed['at'][0]

    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) is None

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:""|list }}' % wcs_objects)
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) is None

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:none|list }}' % wcs_objects)
    context['none'] = None
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) is None

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:"bad-value"|list }}' % wcs_objects)
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) == 'bad-value'

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:"2022-10-17"|list }}' % wcs_objects)
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) == '2022-10-17T00:00:00+02:00'

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:"2022-10-17 23:20"|list }}' % wcs_objects)
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) == '2022-10-17T23:20:00+02:00'

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:a_date|list }}' % wcs_objects)
    context['a_date'] = datetime.date.today()
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) == '2022-10-17T00:00:00+02:00'

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|get_at:a_datetime|list }}' % wcs_objects)
    context['a_datetime'] = datetime.datetime.now()
    t.render(context)
    assert get_at_param(mock_send.call_args_list[0][0][0].url) == '2022-10-17T12:21:00+02:00'


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_errors(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)

    with mock.patch('publik_django_templatetags.wcs.context_processors.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        assert t.render(context) == '[]'

    with mock.patch('publik_django_templatetags.wcs.context_processors.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        requests_get.return_value = mock_resp
        assert t.render(context) == '[]'

    with mock.patch('publik_django_templatetags.wcs.context_processors.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        assert t.render(context) == '[]'

    mock_send.side_effect = lambda *a, **k: MockedRequestResponse(content=json.dumps({'err': 1}))
    assert t.render(context) == '[]'

    mock_send.side_effect = lambda *a, **k: MockedRequestResponse(content=json.dumps({}))
    assert t.render(context) == '[]'

    mock_send.side_effect = lambda *a, **k: MockedRequestResponse(content=json.dumps({'data': None}))
    assert t.render(context) == '[]'


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_access_control(mock_send, wcs_objects, context, nocache):
    # no user in context
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|access_control:request.user|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID=&' in mock_send.call_args_list[0][0][0].url
    assert 'email=&' in mock_send.call_args_list[0][0][0].url

    # current user in anonymous
    context['request'].user = MockAnonymousUser()
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|access_control:request.user|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID=&' in mock_send.call_args_list[0][0][0].url
    assert 'email=&' in mock_send.call_args_list[0][0][0].url

    # current user with uuid
    context['request'].user = MockUserWithNameId()
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|access_control:request.user|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID=xyz&' in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url

    # current user without uuid
    context['request'].user = MockUser()
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|access_control:request.user|list }}' % wcs_objects)
    t.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email=foo%40example.net&' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|access_control:request.user|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_count(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|count }}' % wcs_objects)
    assert t.render(context) == '2'

    context = Context({'foo': None})
    t = Template('{{ foo|count }}')
    assert t.render(context) == '0'


OPERATORS = [
    ('equal', 'eq'),
    ('not_equal', 'ne'),
    ('i_equal', 'ieq'),
    ('less_than', 'lt'),
    ('less_than_or_equal', 'lte'),
    ('greater_than', 'gt'),
    ('greater_than_or_equal', 'gte'),
    ('in', 'in'),
    ('not_in', 'not_in'),
    ('between', 'between'),
]
OPERATORS_WITHOUT_VALUE = [
    ('absent', 'absent'),
    ('existing', 'existing'),
    ('is_today', 'is_today'),
    ('is_tomorrow', 'is_tomorrow'),
    ('is_yesterday', 'is_yesterday'),
    ('is_this_week', 'is_this_week'),
]


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|filter_by:"foo"|filter_value:"bar"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-foo=bar&' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template(
        '{{ %s|objects:"foo"|filter_by:"foo"|filter_value:"bar"|filter_by:"foo2"|filter_value:"bar2"|list }}'
        % wcs_objects
    )
    t.render(context)
    assert 'filter-foo=bar&' in mock_send.call_args_list[0][0][0].url
    assert 'filter-foo2=bar2&' in mock_send.call_args_list[0][0][0].url

    # check boolean
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|filter_by:"foo"|filter_value:True|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-foo=true&' in mock_send.call_args_list[0][0][0].url
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|filter_by:"foo"|filter_value:False|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-foo=false&' in mock_send.call_args_list[0][0][0].url

    # check None
    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|filter_by:"foo"|filter_value:None|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-foo=&' in mock_send.call_args_list[0][0][0].url

    context['foobar'] = None
    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"foo"|%s|filter_value:"bar"|list }}' % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter-foo=bar&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-foo-operator=%s&' % api_op in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ foobar|%s }}' % filter_op)
        t.render(context)
        assert mock_send.call_args_list == []

    for filter_op, api_op in OPERATORS_WITHOUT_VALUE:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|filter_by:"foo"|%s|list }}' % (wcs_objects, filter_op))
        t.render(context)
        assert 'filter-foo=on&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-foo-operator=%s&' % api_op in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ foobar|%s }}' % filter_op)
        t.render(context)
        assert mock_send.call_args_list == []

    mock_send.reset_mock()
    t = Template('{{ foobar|filter_by:"foo"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []

    mock_send.reset_mock()
    t = Template('{{ foobar|filter_value:"foo"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_internal_id(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-internal-id' not in mock_send.call_args_list[0][0][0].url

    for tpl in ['filter_by_internal_id', 'filter_by:"internal_id"|filter_value']:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:None|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-internal-id' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:""|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-internal-id' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:"42"|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-internal-id=42&' in mock_send.call_args_list[0][0][0].url

    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"internal_id"|%s|filter_value:"42"|list }}'
            % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter-internal-id=42&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-internal-id-operator=%s&' % api_op in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|filter_by_internal_id:"42"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_number(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-number' not in mock_send.call_args_list[0][0][0].url

    for tpl in ['filter_by_number', 'filter_by:"number"|filter_value']:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:None|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-number' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:""|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-number' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:"42-35"|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-number=42-35&' in mock_send.call_args_list[0][0][0].url

    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"number"|%s|filter_value:"42-35"|list }}'
            % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter-number=42-35&' in mock_send.call_args_list[0][0][0].url
        # not for this filter
        assert 'filter-number-operator=%s&' % api_op not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|filter_by_number:"42"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_identifier(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url

    for tpl in ['filter_by_identifier', 'filter_by:"identifier"|filter_value']:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:None|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:""|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:"42-35"|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-identifier=42-35&' in mock_send.call_args_list[0][0][0].url

    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"identifier"|%s|filter_value:"42-35"|list }}'
            % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter-identifier=42-35&' in mock_send.call_args_list[0][0][0].url
        # not for this filter
        assert 'filter-identifier-operator=%s&' % api_op not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|filter_by_identifier:"42"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_user(mock_send, wcs_objects, context, nocache):
    context['nameid'] = 'zyx'
    for tpl in ['filter_by_user', 'filter_by:"user"|filter_value']:
        t = Template('{{ %s|objects:"foo"|%s:request.user|list }}' % (wcs_objects, tpl))
        mock_send.reset_mock()
        context['request'].user = None
        t.render(context)
        assert mock_send.call_args_list == []

        context['request'].user = MockAnonymousUser()
        mock_send.reset_mock()
        t.render(context)
        assert mock_send.call_args_list == []

        context['request'].user = MockUser()
        mock_send.reset_mock()
        t.render(context)
        assert mock_send.call_args_list == []

        context['request'].user = MockUserWithNameId()
        mock_send.reset_mock()
        t.render(context)
        assert 'filter-user-uuid=xyz&' in mock_send.call_args_list[0][0][0].url

        t = Template('{{ %s|objects:"foo"|%s:nameid|list }}' % (wcs_objects, tpl))
        mock_send.reset_mock()
        t.render(context)
        assert 'filter-user-uuid=zyx&' in mock_send.call_args_list[0][0][0].url

    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"user"|%s|filter_value:request.user|list }}'
            % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter-user-uuid=xyz&' in mock_send.call_args_list[0][0][0].url
        # not for this filter
        assert 'filter-user-uuid-operator=%s&' % api_op not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|filter_by_user:request.user|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_status(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter=&' not in mock_send.call_args_list[0][0][0].url
    assert 'filter-operator=eq&' not in mock_send.call_args_list[0][0][0].url

    for tpl in ['filter_by_status', 'filter_by:"status"|filter_value']:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:None|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter=&' not in mock_send.call_args_list[0][0][0].url
        assert 'filter-operator=eq&' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:""|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter=&' not in mock_send.call_args_list[0][0][0].url
        assert 'filter-operator=eq&' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:"foobar"|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter=foobar&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-operator=eq&' in mock_send.call_args_list[0][0][0].url

    for filter_op, api_op in OPERATORS:
        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|filter_by:"status"|%s|filter_value:"foobar"|list }}'
            % (wcs_objects, filter_op)
        )
        t.render(context)
        assert 'filter=foobar&' in mock_send.call_args_list[0][0][0].url
        if api_op in ['eq', 'ne']:
            assert 'filter-operator=%s&' % api_op in mock_send.call_args_list[0][0][0].url
        else:
            assert 'filter-operator=%s&' % api_op not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|pending|list }}' % wcs_objects)
    t.render(context)
    assert 'filter=pending&' in mock_send.call_args_list[0][0][0].url
    assert 'filter-operator=eq&' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|done|list }}' % wcs_objects)
    t.render(context)
    assert 'filter=done&' in mock_send.call_args_list[0][0][0].url
    assert 'filter-operator=eq&' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|filter_by_status:"foobar"|list }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_filter_by_distance(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|list }}' % wcs_objects)
    t.render(context)
    assert 'filter-distance=&' not in mock_send.call_args_list[0][0][0].url

    for tpl in ['filter_by_distance', 'filter_by:"distance"|filter_value']:
        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:None|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-distance=&' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:""|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-distance=&' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template('{{ %s|objects:"foo"|%s:"10000"|list }}' % (wcs_objects, tpl))
        t.render(context)
        assert 'filter-distance=&' not in mock_send.call_args_list[0][0][0].url

        mock_send.reset_mock()
        t = Template(
            '{{ %s|objects:"foo"|set_geo_center_lat:1|set_geo_center_lon:2|%s:"10000"|list }}'
            % (wcs_objects, tpl)
        )
        t.render(context)
        assert 'center_lat=1&' in mock_send.call_args_list[0][0][0].url
        assert 'center_lon=2&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-distance=10000&' in mock_send.call_args_list[0][0][0].url
        assert 'filter-distance-operator' not in mock_send.call_args_list[0][0][0].url


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_getlist(mock_send, wcs_objects, context, nocache):
    t = Template('{%% for v in %s|objects:"foo"|getlist:"id" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    assert t.render(context) == '1,2,'
    t = Template('{%% for v in %s|objects:"foo"|getlist:"fields" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    result = t.render(context)
    if '&#39;' in result:
        # django 2.2
        assert result == '{&#39;foo&#39;: &#39;bar&#39;},{&#39;foo&#39;: &#39;baz&#39;},'
    else:
        # django 3.2
        assert result == '{&#x27;foo&#x27;: &#x27;bar&#x27;},{&#x27;foo&#x27;: &#x27;baz&#x27;},'
    t = Template(
        '{%% for v in %s|objects:"foo"|getlist:"fields"|getlist:"foo" %%}{{ v }},{%% endfor %%}' % wcs_objects
    )
    t.render(context)
    assert t.render(context) == 'bar,baz,'
    t = Template(
        '{%% for v in %s|objects:"foo"|getlist:"fields"|getlist:"unknown" %%}{{ v }},{%% endfor %%}'
        % wcs_objects
    )
    t.render(context)
    assert t.render(context) == 'None,None,'


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_order_by(mock_send, wcs_objects, context, nocache):
    t = Template('{%% for v in %s|objects:"foo" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    assert 'order_by' not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{%% for v in %s|objects:"foo"|order_by:"bar" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    assert 'order_by=bar' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{%% for v in %s|objects:"foo"|order_by:"-bar" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    assert 'order_by=-bar' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{%% for v in %s|objects:"foo"|order_by:"" %%}{{ v }},{%% endfor %%}' % wcs_objects)
    t.render(context)
    assert 'order_by' not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template(
        '{%% for v in %s|objects:"foo"|order_by:"bar"|order_by:"" %%}{{ v }},{%% endfor %%}' % wcs_objects
    )
    t.render(context)
    assert 'order_by' not in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template(
        '{%% for v in %s|objects:"foo"|order_by:""|order_by:"bar" %%}{{ v }},{%% endfor %%}' % wcs_objects
    )
    t.render(context)
    assert 'order_by=bar' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    context['foobar'] = None
    t = Template('{{ foobar|order_by:"foo" }}')
    t.render(context)
    assert mock_send.call_args_list == []


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_first(mock_send, wcs_objects, context, nocache):
    t = Template('{{ %s|objects:"foo"|get:0|get:"id" }}' % wcs_objects)
    t.render(context)
    assert t.render(context) == '1'
    assert 'list?orig=combo&algo' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    t = Template('{{ %s|objects:"foo"|first|get:"id" }}' % wcs_objects)
    t.render(context)
    assert t.render(context) == '1'
    assert 'list?orig=combo&limit=1&algo' in mock_send.call_args_list[0][0][0].url

    mock_send.reset_mock()
    mock_send.side_effect = lambda *a, **k: MockedRequestResponse(content=json.dumps({'data': []}))
    t = Template('{{ %s|objects:"foo"|first }}' % wcs_objects)
    t.render(context)
    assert t.render(context) == ''


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_cache(mock_send, wcs_objects, settings, context, django_cache, freezer):
    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 1

    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 1

    django_cache.clear()

    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 2

    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 2

    freezer.tick(4)

    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 2

    # cache duration is 5 seconds
    freezer.tick(1.1)

    t = Template('{%% for card in %s|objects:"foo" %%}{{ card.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 3


@pytest.mark.parametrize('wcs_objects', ['cards', 'forms'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_nocache(mock_send, wcs_objects, settings, context, django_cache, freezer):
    t = Template('{%% for form in %s|objects:"foo" %%}{{ form.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 1

    context['request'] = RequestFactory().get('/?nocache')
    context[wcs_objects] = (Cards if wcs_objects == 'cards' else Forms)(context['request'])
    context['request'].user = MockAnonymousUser()

    t = Template('{%% for form in %s|objects:"foo" %%}{{ form.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 1

    context['request'].user = MockUser()

    t = Template('{%% for form in %s|objects:"foo" %%}{{ form.id }} {%% endfor %%}' % wcs_objects)
    assert t.render(context) == '1 2 '
    assert mock_send.call_count == 2


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_requests_timeout(mock_send, settings, context, nocache):
    t = Template('{{ forms|objects:"foo"|count }}')
    assert t.render(context) == '2'
    assert mock_send.call_args[1]['timeout'] == 25

    context['request'].requests_timeout = 10
    assert t.render(context) == '2'
    assert mock_send.call_args[1]['timeout'] == 10


def test_requests_max_retries(settings, context, nocache):
    t = Template('{{ forms|objects:"foo"|filter_by:"foo"|filter_value:"bar"|count }}')

    with mock.patch('requests.Session.send') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 504
        requests_get.return_value = mock_resp
        assert t.render(context) == '0'
        assert len(requests_get.call_args_list) == 1

    # max 5 retry
    context['request'].requests_max_retries = 5
    with mock.patch('requests.Session.send') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 504
        requests_get.return_value = mock_resp
        assert t.render(context) == '0'
        assert len(requests_get.call_args_list) == 6

    with mock.patch('requests.Session.send') as requests_get:
        requests_get.side_effect = Timeout()
        assert t.render(context) == '0'
        assert len(requests_get.call_args_list) == 6

    # other errors, just one call
    with mock.patch('requests.Session.send') as requests_get:
        requests_get.side_effect = ConnectionError()
        assert t.render(context) == '0'
        assert len(requests_get.call_args_list) == 1

    with mock.patch('requests.Session.send') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 400
        requests_get.return_value = mock_resp
        assert t.render(context) == '0'
        assert len(requests_get.call_args_list) == 1


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_cache_duration(mock_send, requests_get, settings, context, nocache):
    t = Template('{{ forms|objects:"foo"|count }}')
    assert t.render(context) == '2'
    assert requests_get.call_args[1]['cache_duration'] == 5

    t = Template('{{ forms|objects:"foo"|cache_duration:30|count }}')
    assert t.render(context) == '2'
    assert requests_get.call_args[1]['cache_duration'] == 30
