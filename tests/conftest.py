import unittest.mock as mock

import pytest


@pytest.fixture
def nocache(settings):
    settings.CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
    }


@pytest.fixture
def django_cache():
    from django.core.cache import cache

    cache.clear()
    return cache


@pytest.fixture
def requests_get():
    from publik_django_templatetags.utils.requests_wrapper import requests

    with mock.patch.object(requests, 'get', wraps=requests.get) as wrapped_get:
        yield wrapped_get
