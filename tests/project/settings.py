import os

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.postgresql_psycopg2'),
        'NAME': 'publik-django-templatetags-test-%s'
        % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:45],
    }
}

KNOWN_SERVICES = {
    'wcs': {
        'default': {
            'title': 'test',
            'url': 'http://127.0.0.1:8999/',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://127.0.0.1:8999/backoffice/',
            'secondary': False,
        },
        'other': {
            'title': 'test2',
            'url': 'http://127.0.0.2:8999/',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://127.0.0.2:8999/backoffice/',
            'secondary': True,
        },
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'publik_django_templatetags.wcs.context_processors.cards',
            ],
            'builtins': [
                'publik_django_templatetags.publik.templatetags.publik',
                'publik_django_templatetags.wcs.templatetags.wcs',
            ],
        },
    },
]

REQUESTS_TIMEOUT = 25

DEBUG = True
USE_TZ = True
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
]
STATIC_URL = '/static/'
SITE_ID = 1
MIDDLEWARE_CLASSES = ()
LOGGING = {}
SECRET_KEY = 'yay'
